import React, { useContext } from "react";
import { ThemeContext } from "../context/ThemeContext";
import { AuthContext } from "../context/AuthContext";
const Navbar = () => {
  //   const testContext = useContext(ThemeContext);
  //   console.log(testContext);
  // vì testCOntext ở trên chính là theme vì vậy ta sẽ móc trực tiếp nó như dưới
  const { theme } = useContext(ThemeContext);
  const { isLightTheme, light, dark } = theme;
  const style = isLightTheme ? light : dark;

  const { isAuthenticated, toggleAuth } = useContext(AuthContext);

  return (
    <div className="navbar" style={style}>
      <h1>My Hooks App</h1>
      <ul>
        <li>Home</li>
        <li>About</li>
        <li>
          {isAuthenticated ? "you are login" : ""}
          <button onClick={toggleAuth}>
            {isAuthenticated ? "Logout" : "login"}
          </button>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;
