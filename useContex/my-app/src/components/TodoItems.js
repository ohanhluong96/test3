import React, { useContext } from "react";
import { ThemeContext } from "../context/ThemeContext";
import { TodoContext } from "../context/TodoContext";
import { DELETE_TODO } from "../reducers/types";
const TodoItems = ({ todo }) => {
  const { theme } = useContext(ThemeContext);
  const { isLightTheme, light, dark } = theme;
  const style = isLightTheme ? light : dark;
  const { dispatch } = useContext(TodoContext);
  return (
    <li
      onClick={() => {
        dispatch({
          type: DELETE_TODO,
          payload: {
            id: todo.id,
          },
        });
      }}
      style={style}
    >
      {todo.title}
    </li>
  );
};

export default TodoItems;
