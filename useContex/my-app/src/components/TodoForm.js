import React, { useState, useContext } from "react";
import { ThemeContext } from "../context/ThemeContext";
import { TodoContext } from "../context/TodoContext";
import { ADD_TODO } from "../reducers/types";
const TodoForm = () => {
  const [title, setTitle] = useState("");
  const onTitleChange = (e) => {
    setTitle(e.target.value);
  };
  const { dispatch } = useContext(TodoContext);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: ADD_TODO,
      payload: {
        todo: {
          id: Math.floor(Math.random() * 10000),
          title,
        },
      },
    });
    setTitle("");
  };

  const { theme } = useContext(ThemeContext);
  const { isLightTheme, light, dark } = theme;
  const style = isLightTheme ? light : dark;

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Enter your todo"
        onChange={onTitleChange}
        value={title}
        required
      />
      <input type="submit" value="add" style={style} />
    </form>
  );
};

export default TodoForm;
