import React, { useContext } from "react";
import TodoForm from "./TodoForm";
import TodoItems from "./TodoItems";
import { TodoContext } from "../context/TodoContext";
import { AuthContext } from "../context/AuthContext";
const Todos = () => {
  const { todos } = useContext(TodoContext);
  const { isAuthenticated } = useContext(AuthContext);
  return (
    <div className="todo-list">
      <TodoForm />
      {isAuthenticated ? (
        <ul>
          {todos.map((todo) => {
            return <TodoItems todo={todo} key={todo.id} />;
          })}
        </ul>
      ) : (
        <p>not Authorize</p>
      )}
    </div>
  );
};

export default Todos;
