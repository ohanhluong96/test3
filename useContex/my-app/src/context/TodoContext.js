import React, { Children, createContext, useEffect, useReducer } from "react";
import { todoReducer } from "../reducers/TodoReducer";
import { GET_TODO, SAVE_TODO } from "../reducers/types";

export const TodoContext = createContext();

const TodoContextProvider = ({ children }) => {
  // const [todos, setTodos] = useState([]);
  // lấy dữ liệu từ localStorage

  const [todos, dispatch] = useReducer(todoReducer, []);

  useEffect(() => {
    dispatch({
      type: GET_TODO,
      payload: null,
    });
    // const todos = localStorage.getItem("todos");
    // if (todos) setTodos(JSON.parse(todos));
    // vì value trong localStorage là dang string ta .parse để đưa về dạng array
    //
  }, []);

  useEffect(() => {
    dispatch({
      type: SAVE_TODO,
      payload: { todos },
    });

    // localStorage.setItem("todos", JSON.stringify(todos));
    // bởi vì localstorgae chỉ nhận string nên ta dùng
    // Json.stringify biến tất cả thành string dạng Json
  }, [todos]);

  /*
  const addTodo = (todo) => {
    setTodos([todo, ...todos]);
  };
  const deleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  }; */

  const TodoContextData = {
    todos,
    // addTodo,
    // deleteTodo,
    dispatch,
  };

  return (
    <TodoContext.Provider value={TodoContextData}>
      {children}
    </TodoContext.Provider>
  );
};
export default TodoContextProvider;
