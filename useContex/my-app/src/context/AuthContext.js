import React, { Children, createContext, useEffect, useState } from "react";

export const AuthContext = createContext();
const AuthContextProvider = ({ children }) => {
  const [isAuthenticated, setAuthentication] = useState(false);

  const toggleAuth = () => {
    setAuthentication(!isAuthenticated);
  };

  useEffect(() => {
    alert(isAuthenticated ? "Login Successfull" : "pls login to use");
  }, [isAuthenticated]);

  const authenContextData = {
    isAuthenticated,
    toggleAuth,
  };
  return (
    <AuthContext.Provider value={authenContextData}>
      {children}
    </AuthContext.Provider>
  );
};
export default AuthContextProvider;
