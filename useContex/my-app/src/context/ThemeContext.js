import React, { createContext, useState } from "react";

export const ThemeContext = createContext();
// taoj kho luuu tru
const ThemeContextProvider = ({ children }) => {
  const [theme, setTheme] = useState({
    isLightTheme: true,
    Light: {
      background: "rgb(240,240,240)",
      color: "black",
    },
    dark: {
      background: "rgb( 39,39,39)",
      color: "white",
    },
  });
  // Function toggle theme
  const toggleTheme = () => {
    setTheme({
      ...theme,
      isLightTheme: !theme.isLightTheme,
    });
  };

  //   context data
  const themeContextData = {
    theme,
    toggleTheme,
  };
  // return provider
  return (
    <ThemeContext.Provider value={themeContextData}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContextProvider;
